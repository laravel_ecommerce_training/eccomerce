<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('brand')->nullable();
            $table->text('description');

            $table->integer('price');
            $table->integer('quantity');
            $table->float('discount')->nullable();

            $table->integer('cat_id');
            $table->integer('sub_cat_id')->nullable();

            $table->string('slug');
            $table->string('status');

            $table->text('product_image');
            $table->integer('vendor_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
