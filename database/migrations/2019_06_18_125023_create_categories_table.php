<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('title');
            $table->text('summary');

            $table->boolean('is_parent');
            $table->unsignedBigInteger('parent_id')->nullable();

            $table->string('status');

            $table->unsignedBigInteger('added_by')->nullable();


            $table->foreign('added_by')->references('id')->on('users')->onDelete('set null');
//            $table->foreign('parent_id')->references('id')->on('categories')->onDelete('set null');

            $table->text('category_image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
