
@if(Session::has('success'))
<div class="alert alert-success alert-dismissable remove-message">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    {{Session::get('success')}}

</div>
@elseif(Session::has('error'))
    <div class="alert alert-danger alert-dismissable remove-message">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        {{Session::get('error')}}
    </div>
@endif


<script>
    setTimeout(function(){$('.remove-message').slideUp();},5000);  //slidup after 3 second
</script>


