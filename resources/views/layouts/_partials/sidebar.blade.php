<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav slimscrollsidebar">
        <div class="sidebar-head">
            <h3><span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i class="ti-close visible-xs"></i></span> <span class="hide-menu">Navigation</span></h3> </div>
        <ul class="nav" id="side-menu">

            {{--<li>--}}
                {{--<a href="inbox.html" class="waves-effect"><i class="mdi mdi-apps fa-fw"></i> <span class="hide-menu">Apps<span class="fa arrow"></span></span></a>--}}
                {{--<ul class="nav nav-second-level">--}}
                    {{--<li><a href="chat.html"><i class="ti-comments-smiley fa-fw"></i><span class="hide-menu">Chat-message</span></a></li>--}}
                    {{--<li><a href="javascript:void(0)" class="waves-effect"><i class="ti-desktop fa-fw"></i><span class="hide-menu">Inbox</span><span class="fa arrow"></span></a>--}}
                        {{--<ul class="nav nav-third-level">--}}
                            {{--<li> <a href="inbox.html"><i class="ti-email fa-fw"></i><span class="hide-menu">Mail box</span></a></li>--}}
                            {{--<li> <a href="inbox-detail.html"><i class="ti-layout-media-left-alt fa-fw"></i><span class="hide-menu">Inbox detail</span></a></li>--}}
                            {{--<li> <a href="compose.html"><i class="ti-layout-media-center-alt fa-fw"></i><span class="hide-menu">Compose mail</span></a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li><a href="javascript:void(0)" class="waves-effect"><i class="ti-user fa-fw"></i><span class="hide-menu">Contacts</span><span class="fa arrow"></span></a>--}}
                        {{--<ul class="nav nav-third-level">--}}
                            {{--<li> <a href="contact.html"><i class="icon-people fa-fw"></i><span class="hide-menu">Contact1</span></a></li>--}}
                            {{--<li> <a href="contact2.html"><i class="icon-user-follow fa-fw"></i><span class="hide-menu">Contact2</span></a></li>--}}
                            {{--<li> <a href="contact-detail.html"><i class="icon-user-following fa-fw"></i><span class="hide-menu">Contact Detail</span></a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}

            <li>
            <a href="inbox.html" class="waves-effect"><i class="mdi mdi-apps fa-fw"></i> <span class="hide-menu">Users<span class="fa arrow"></span></span></a>
            <ul class="nav nav-second-level">
            <li>
                <a href="{{route('register')}}">
                    <i class="ti-comments-smiley fa-fw"></i>
                    <span class="hide-menu">
                        Add User
                    </span>
                </a>
            </li>
            <li>
                <a href="{{route('admin.user.list')}}"><i class="ti-list fa-fw"></i>
                    <span class="hide-menu">List User</span>
                </a>
            </li>

            </ul>
            </li>

            <li class="devider"></li>
            @can('banner')
            <li> <a href="{{route('admin.banner.list')}}" class="waves-effect"><i  class="mdi mdi-home fa-fw"></i> <span class="hide-menu">Banner Management</span></a> </li>
            @endcan
            <li class="devider"></li>

            <li> <a href="{{route('admin.category.list')}}" class="waves-effect"><i  class="mdi mdi-blinds"></i> <span class="hide-menu"> Category Management</span></a> </li>

            <li class="devider"></li>

            <li> <a href="{{route('admin.product.list')}}" class="waves-effect"><i  class="mdi mdi-package-variant-closed fa-fw"></i> <span class="hide-menu">Product Management</span></a> </li>
            <li> <a href="widgets.html" class="waves-effect"><i  class="mdi mdi-package-variant-closed fa-fw"></i> <span class="hide-menu">Widgets</span></a> </li>


            {{--<li><a href="login.html" class="waves-effect"><i class="mdi mdi-logout fa-fw"></i> <span class="hide-menu">Log out</span></a></li>--}}

        </ul>
    </div>
</div>