<!DOCTYPE html>
<html lang="en">

@include('layouts._partials.head')

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    {{--nav bar--}}
    @include('layouts._partials.nav')

    {{--sidebar left start --}}
    @include('layouts._partials.sidebar')
    <!-- End Left Sidebar -->

    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">
                        @yield('page_heading')
                    </h4>
                </div>

                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12" style="text-align: right;">
                    @yield('button')
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <div class="row">
               @include('layouts._partials.message')
            </div>


            @yield('content')

        </div>
        <!-- /.container-fluid -->
        @include('layouts._partials.footer')

    </div>
</div>
@include('layouts._partials.scripts')
</body>

</html>
