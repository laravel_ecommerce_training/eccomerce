
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
    <title>Ample Admin Template - The Ultimate Multipurpose admin template</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{asset(STATIC_DIR.'assets/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- animation CSS -->
    <link href="{{asset(STATIC_DIR.'assets/css/animate.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{asset(STATIC_DIR.'assets/css/style.css')}}" rel="stylesheet">
    <!-- color CSS -->
    <link href="{{asset(STATIC_DIR.'assets/css/colors/blue.css')}}" id="theme"  rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .new-login-register  {
            background: url({{ asset(STATIC_DIR.'assets/plugins/images/login-register.jpg') }}) center center/cover no-repeat!important;
            width: 500px;
            height: 100%;
            position: fixed;
        }


    </style>
</head>
<body>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="new-login-register">
    <div class="login-box login-sidebar">
        <div class="white-box">
            <form class="form-horizontal form-material" method="post" action="{{route('login')}}">
                @csrf
                <a href="javascript:void(0)" class="text-center db"><img src="{{asset(STATIC_DIR.'assets/plugins/images/admin-logo-dark.png')}}" alt="Home" />
                    <br/><img src="{{asset(STATIC_DIR.'assets/plugins/images/admin-text-dark.png')}}" alt="Home" /></a>

                <div class="form-group m-t-40">
                    <div class="col-xs-12">
                        <input class="form-control" name="email" type="text" required="" placeholder="Enter Your Email">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" name="password" required="" placeholder="Password">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <div class="checkbox checkbox-primary pull-left p-t-0">
                            <input id="checkbox-signup" type="checkbox">
                            <label for="checkbox-signup"> Remember me </label>
                        </div>
                    </div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
                    </div>
                </div>

            </form>

        </div>
    </div>
</section>
<!-- jQuery -->
<script src="{{asset(STATIC_DIR.'assets/plugins/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{asset(STATIC_DIR.'assets/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- Menu Plugin JavaScript -->
{{--{{asset(STATIC_DIR.'assets/')}}--}}
<script src="{{asset(STATIC_DIR.'assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js')}}"></script>

<!--slimscroll JavaScript -->
<script src="{{asset(STATIC_DIR.'assets/js/jquery.slimscroll.js')}}"></script>
<!--Wave Effects -->
<script src="{{asset(STATIC_DIR.'assets/js/waves.js')}}"></script>
<!-- Custom Theme JavaScript -->
<script src="{{asset(STATIC_DIR.'assets/js/custom.min.js')}}"></script>
<!--Style Switcher -->
<script src=""{{asset(STATIC_DIR.'assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js')}}></script>
</body>
</html>
