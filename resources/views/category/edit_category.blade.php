@extends('layouts.app')


@section('page_title')
    Category Edit
@endsection

@section('page_heading')
    Category Edit
@endsection

@section('css')
    <link rel="stylesheet" href="{{asset(STATIC_DIR.'assets/plugins/bower_components/dropify/dist/css/dropify.min.css')}}">
@endsection

@section('button')
    <a href="{{route("admin.category.list")}}" class="btn btn-primary"><i class="fa fa-list"> Category List</i></a>

@endsection

@section('content')
    <div class="row">
        <div class="white-box">
            @if($errors->has('edit_error'))
                <p style="color:red;">{{$errors->first('edit_error')}}</p>
            @endif
            <form class="form-horizontal" enctype="multipart/form-data" action="{{route('admin.category.store-category')}}" method="post">
                @csrf
                <input type="hidden" name="category_id" value="{{$category->id}}">
                <div class="form-group">
                    <label class="col-md-12">Title</label>
                    <div class="col-md-12">
                        <input type="text" class="form-control" name="title" value="{{old('title') ?? $category->title}}" placeholder="Enter Banner Title"> </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12" for="">Summary</label>
                    <div class="col-md-12">
                        <textarea name="summary" class="form-control"  cols="30" rows="10" placeholder="Enter Summary">{{$category->summary}}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <input type="checkbox" name="is_parent" id="is_parent" @if($category->is_parent == 1) checked @endif>
                        <label class="">Is Parent</label>
                    </div>
                </div>

                <div class="form-group" id="parent_category">
                    <label class="col-md-12">Sub Category</label>
                    <div class="col-md-12">
                        <select name="parent_id" id="" class="form-control">
                            <option value="">Select Any One</option>
                            @foreach($parent as $value)
                                <option value="{{$value->id}}" @if($value->id == $category->parent_id) selected @endif>{{$value->title}}</option>
                            @endforeach

                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12">Status</label>
                    <div class="col-md-12">
                        <select name="status" id="" class="form-control">
                            <option value="">Select Any One</option>
                            <option value="published" @if($category->status == 'published') selected @endif>Published</option>
                            <option value="unpublished" @if($category->status == 'unpublished') selected @endif>Unpublished</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="input-file-now" class="col-md-12">Category Image</label>
                    <img src="{{ asset('public/storage/'.$category->category_image)}}" class="img img-responsive" style="max-width: 100px; alt="">
                    <input type="file" name="category_image" id="input-file-now" class="dropify"/>
                </div>

                <div class="form-group">
                    <input type="submit" value="Edit Category" class="btn btn-success">
                </div>


            </form>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset(STATIC_DIR.'assets/plugins/bower_components/dropify/dist/js/dropify.min.js')}}"></script>
    <script>
        $('.dropify').dropify();

        $('#is_parent').change(function(){
            let temp = $('#is_parent').prop('checked');
            // alert(temp);
            if(temp){
                $('#parent_category').slideUp();
            }
            else{
                $('#parent_category').slideDown();

            }
        })

        $(document).ready(function(){
            let temp = $('#is_parent').prop('checked');
            // alert(temp);
            if(temp){
                $('#parent_category').slideUp();
            }
            else{
                $('#parent_category').slideDown();

            }
        })
    </script>
@endsection