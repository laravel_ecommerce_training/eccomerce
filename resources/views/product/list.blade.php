@extends('layouts.app')

@section('css')
    <link href="{{ asset(STATIC_DIR."assets/plugins/bower_components/datatables/jquery.dataTables.min.css") }}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('page_title')
    Product Listing
@endsection

@section('page_heading')
    Product List
@endsection

@section('button')
    <a href="{{route("admin.product.show-product-form")}}" class="btn btn-primary"><i class="fa fa-plus"> Product Add</i></a>

@endsection

@section('content')

    <div class="row ">
        <div class="white-box">
            <table class="table table-bordered  " id="user-datatable">
                <thead>
                    <th>S.N.</th>
                    <th>Title</th>
                    <th>Brand</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Status</th>
                    <th>Product Image</th>
                    <th>Action</th>
                </thead>

                <tbody>
                    @foreach($product as $value)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{ $value->title }}</td>
                            <td> {{ $value->brand }} </td>
                            <td> {{ $value->price }}</td>
                            <td> {{ $value->quantity }}</td>
                            <td> {{$value->status}} </td>
                            <td> 
                                  <img src="{{asset(STATIC_DIR.'storage/'.$value->product_image)}}" alt="" style="max-width: 100px;" class="img img-responsive">
                            </td>

                            <td>Edit || Delete</td>
                        </tr>
                    @endforeach
            
                </tbody>
            </table>
        </div>
    </div>
    @include('category.modal')

@endsection


@section('scripts')
    <script src="{{ asset(STATIC_DIR.'assets/plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>


    <script>
        $(document).ready(function() {
            $('#user-datatable').DataTable();
        });



        $('#deleteCategory').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var id = button.data('id');
            var title = button.data('title');
            // var recipient = button.data('whatever') // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('#category_id').val(id);
            modal.find('.title').text(title);
        })
    </script>
@endsection