@extends('layouts.app')


@section('page_title')
    Banner Listing
@endsection

@section('page_heading')
    Banner Listing
@endsection

@section('css')
    <link rel="stylesheet" href="{{asset(STATIC_DIR.'assets/plugins/bower_components/dropify/dist/css/dropify.min.css')}}">
@endsection

@section('button')
    <a href="{{route("admin.banner.list")}}" class="btn btn-primary"><i class="fa fa-list"> Banner List</i></a>

@endsection

@section('content')
    <div class="row">
        <div class="white-box">
            <form class="form-horizontal" enctype="multipart/form-data" action="{{route('admin.banner.insert-banner')}}" method="post">
                @csrf
                <div class="form-group">
                    <label class="col-md-12">Banner Title</label>
                    <div class="col-md-12">
                        <input type="text" class="form-control" name="title" placeholder="Enter Banner Title" value="{{old('title')}}">
                        @if($errors->has('title'))
                            <span class="invalid-feedback" role="alert">
                                    <p style="color:red;">{{ $errors->first('title') }}</p>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12" for="">Summary</label>
                    <div class="col-md-12">
                        <textarea name="summary" class="form-control"  cols="30" rows="10"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12">Link</label>
                    <div class="col-md-12">
                        <input type="text" name="link" class="form-control" placeholder="Enter Link"> </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12">Status</label>
                    <div class="col-md-12">
                        <select name="status" id="" class="form-control">
                            <option value="">Select Any One</option>
                            <option value="active">Active</option>
                            <option value="inactive">InActive</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                        <label for="input-file-now" class="col-md-12">Banner Image</label>
                        <input type="file" name="banner_image" id="input-file-now" class="dropify"/>
                </div>

                <div class="form-group">
                    <input type="submit" value="Add Banner" class="btn btn-success">
                </div>


            </form>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset(STATIC_DIR.'assets/plugins/bower_components/dropify/dist/js/dropify.min.js')}}"></script>
    <script>
        $('.dropify').dropify();
    </script>
@endsection