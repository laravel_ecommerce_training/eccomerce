<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if(!defined('STATIC_DIR')) define('STATIC_DIR','public/');
Route::get('/','HomeController@index');

Auth::routes();

//example of front end user login
//Route::get('/frontend/login','Auth\LoginController@showUserLogin')->name('user.login');

Route::group(['prefix' => 'admin' , 'middleware' => 'auth' , 'as' => 'admin.' ],function(){

    Route::get('/dashboard','HomeController@showDashboard')->name('dashboard')->middleware('auth');
    Route::get('/dashboard','HomeController@showDashboard')->name('dashboard');



//user crud operation routes
    //user routes
    Route::group(['prefix' => 'user','as' => 'user.'],function(){
        Route::get('/', ['uses' => 'UserController@list' , 'as' => 'list']);
//        Route::get('/user/list','UserController@list')->name('user.list');
        Route::get('/update/{id}','UserController@edit')->name('edit');
        Route::post('/update','UserController@updateUser')->name('update');
        Route::post('/delete','UserController@deleteUser')->name('delete');

        Route::post('/user/register','UserController@registerUser')->name('register');
    });
//end user routes

    //banner routes
    Route::group(['prefix' => 'banner','as' => 'banner.'],function(){
        Route::get('/list', ['uses' => 'BannerController@list' , 'as' => 'list']);
        Route::get('/', ['uses' => 'BannerController@addBanner' , 'as' => 'add-banner']);
        Route::post('/add', ['uses' => 'BannerController@postAddBanner' , 'as' => 'insert-banner']);
    });
    //end banner routes

    //category  routes
    Route::group(['prefix' => 'category','as' => 'category.'],function(){
        Route::get('/list', ['uses' => 'CategoryController@list' , 'as' => 'list']);
        Route::get('/add', ['uses' => 'CategoryController@showCategoryForm' , 'as' => 'show-category-form']);
        Route::post('/add', ['uses' => 'CategoryController@postAddCategory' , 'as' => 'add-category']);
        Route::get('/edit/{id}', ['uses' => 'CategoryController@getEditCategory' , 'as' => 'edit-category']);
        Route::post('/edit', ['uses' => 'CategoryController@postEditCategory' , 'as' => 'store-category']);

        //ajax routes
        Route::post('/category/id', ['uses' => 'CategoryController@getCategoryById' , 'as' => 'get-sub-category-by-id']);



        Route::post('/delete', ['uses' => 'CategoryController@deleteCategory' , 'as' => 'delete-category']);
    });
//    end category routes

    //product  routes
    Route::group(['prefix' => 'product','as' => 'product.'],function(){
        Route::get('/list', ['uses' => 'ProductController@list' , 'as' => 'list']);
        Route::get('/add', ['uses' => 'ProductController@showProductForm' , 'as' => 'show-product-form'])->middleware('can:banner');
       Route::post('/add', ['uses' => 'ProductController@postAddCategory' , 'as' => 'add-product']);
//        Route::get('/edit/{id}', ['uses' => 'CategoryController@getEditCategory' , 'as' => 'edit-category']);
//        Route::post('/edit', ['uses' => 'CategoryController@postEditCategory' , 'as' => 'store-category']);


        Route::post('/delete', ['uses' => 'CategoryController@deleteCategory' , 'as' => 'delete-category']);
    });
//    end product routes


});

Route::get('/home', 'HomeController@index')->name('home');


//end user crud operation routes
//Route::group(['prefix' => 'user'], function({
//    Route::get('/list','UserController@list')->name('user.list');
//    Route::get('/edit','UserController@edit')->name('user.edit');
//
//});
//Route::get('/user/edit','UserController@edit')->name('user.edit');
//Route::get('/user/delete','UserController@delete')->name('user.delete');