<?php

namespace App\Http\Controllers;

use App\Banner;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $this->middleware('guest');
        $data['banner'] = Banner::limit(3)
                            ->orderBy('id','desc')
                            ->where('status','active')
                            ->get();
//        dd($data);
        return view('home',$data);
    }

    public function user(){
        return view('user');
    }

    public function showDashboard(){
        return view('dashboard');
    }
}
