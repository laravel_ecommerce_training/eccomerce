<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banner;

class BannerController extends Controller
{
    public function list(){
        $data['banner'] = Banner::all();
//        dd($data);
       return view('banner.list_banner',$data);
    }
    public function addBanner(){
        return view('banner.add_banner_form');
    }

    public function postAddBanner(Request $request){
        $request->validate([
           'title' => 'required|max:255',
           'summary' => 'required',
           'link' => 'required',
           'status' => 'required|string',
           'banner_image' => 'required|mimes:jpeg,png,jpg',
        ]);

        $banner = new Banner();
        $banner->title = $request->title;
        $banner->summary = $request->summary;
        $banner->link = $request->link;
        $banner->status = $request->status;

        $banner->banner_image = $request->file('banner_image')->store('banners','public');

        $banner->save();

        \Session::flash('success','Banner Added Successfully');

        return redirect()->route('admin.banner.list');


    }
}
