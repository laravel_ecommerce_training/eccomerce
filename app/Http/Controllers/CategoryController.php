<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    public function list()
    {
        $data['category'] = Category::with('user', 'getParentNameById')->get();
//        dd($data);
        return view('category.list', $data);
    }

    public function showCategoryForm()
    {
        $data['category'] = Category::where('is_parent', 1)->get();
        return view('category.category_form', $data);
    }

    public function postAddCategory(Request $request)
    {

        $request->validate([
            'title' => 'required|max:255',
            'summary' => 'required',
            'status' => 'required',
            'category_image' => 'required|mimes:jpg,png,jpeg',
        ]);
        $category = new Category();
        $category->title = $request->title;
        $category->summary = $request->summary;

        if (isset($request->is_parent)) {
            $category->is_parent = 1;
        } else {
            $category->is_parent = 0;
        }

        if ($request->parent_id == null) {
            $category->parent_id = 0;
        } else {
            $category->parent_id = $request->parent_id;

        }

        $category->status = $request->status;
        $category->added_by = \Auth::user()->id;


        if (($request->category_image != null)) {
            $path = $request->file('category_image')->store('category', 'public');
        }
        $category->category_image = $path;
        $category->created_at = date('Y-m-d H:i:s');
        $category->save();

        return redirect()->route('admin.category.list');
    }

    public function getEditCategory($id){
        $data['category'] = Category::find($id);
        $data['parent'] = Category::where('is_parent',1)->get();
        return view('category.edit_category',$data);
    }

    public function postEditCategory(Request $request){

//        dd($request->all());

        $request->validate([
            'title' => 'required|max:255',
            'summary' => 'required',
            'status' => 'required',
            'category_image' => 'nullable|mimes:jpg,png,jpeg',
            'category_id' => 'required|integer',
        ]);
//        dd($request->all());
//        dd('validated');

        $category =  Category::find($request->category_id);

        //find sub child if it is parent.
        $sub_category = Category::where('parent_id',$category->id)->get();
//        dd($sub_category);

        $category->title = $request->title;
        $category->summary = $request->summary;

        if (isset($request->is_parent)) {
            $category->is_parent = 1;
            $category->parent_id = 0;
        } else {

            if($sub_category->count() > 0){
                return redirect()->back()->withErrors(['edit_error' => 'Sorry! You cannot make your parent to sub child']);
            }
            $category->is_parent = 0;
            $category->parent_id = $request->parent_id;

        }

        $category->status = $request->status;

        if (($request->category_image != null)) {
            Storage::delete('public/'.$category->category_image);
            $path = $request->file('category_image')->store('category', 'public');
            $category->category_image = $path;
        }

        $category->updated_at = date('Y-m-d H:i:s');
        $category->save();

        \Session::flash('success','Category Edited Successfully');
        return redirect()->route('admin.category.list');
    }

    public function deleteCategory(Request $request)
    {
        $category = Category::find($request->category_id);
        //check if it is parent
        if ($category->is_parent == 1) {
            $sub_child = Category::where('parent_id', $category->id)->get(['id']);
//            dd($sub_child);
            if ($sub_child->count() > 0) {
                foreach ($sub_child as $value) {
                    $temp = Category::find($value->id);
                    $temp->parent_id = null;
                    $temp->save();
                }
            }
        }

        Storage::delete('public/'.$category->category_image);
        $category->delete();
        \Session::flash('success', 'Category Deleted Successfully');

        return redirect()->route('admin.category.list');
    }

    //ajax call
    public function getCategoryById(Request $request){
        $category = Category::where('parent_id',$request->id)->get();

        if($category->count() > 0){
//            return $category;
            return json_encode($category);
        }
        else{
            $category = null;
            return json_encode($category);
        }
    }

}
