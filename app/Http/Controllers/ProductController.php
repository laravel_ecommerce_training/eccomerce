<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\ProductImage;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function list(){
    	$data['product'] = Product::all();
    	// dd($data);
        return view('product.list',$data);
    }

    public function showProductForm(){
        $data['category'] = Category::where('is_parent',1)->get();

//        $data['sub_category'] = Category::where('is_parent',0)->get();
//        dd($data);
        return view('product.add_product',$data);
    }

    public function postAddCategory(Request $request){
//        dd($request->all());
    	$request->validate([
    		'title' => 'required|max:50',
    		'brand' => 'required|max:50',
    		'price' => 'required',

    		'quantity' => 'required',
    		'discount' => 'nullable',
    		'status' => 'required',

    		'description' => 'required',
    		'category' => 'required',
    		'sub_category' => 'nullable',
    		'product_image' => 'required',
    	]);

    	$product = new Product();
    	$product->title = $request->title;
    	$product->brand = $request->brand;
    	$product->price = $request->price;

    	$product->quantity = $request->quantity;
    	$product->discount = $request->discount;
    	$product->description = htmlentities($request->description);

    	$product->cat_id = $request->category;
    	$product->status = $request->status;

    	if(isset($request->sub_category)){
    		$product->sub_cat_id = $request->sub_category;    		
    	}

    	$product->vendor_id = 1;
    	
    	$slug =  str_slug($request->title, '-');
    	$product->slug = $slug.'html';


    	//product image upload
		if(($request->product_image != null)){
            $product->product_image = $request->file('product_image')->store('product','public');
        }

    	$product->save();


		foreach($request->other_images as $value){
            $product_image = new ProductImage();
            $product_image->product_id = $product->id;
		    $product_image->image =  $value->store('product','public');
		    $product_image->save();
        }

    	\Session::flash('success','Product Added Successfully');
    	return redirect()->route('admin.product.list');
    }
}
